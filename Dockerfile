FROM node:10.16-alpine

WORKDIR /app

COPY . .

RUN ls src/index.js && npm install

CMD ["npm", "start"]
