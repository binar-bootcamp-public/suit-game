const mongoose = require('mongoose');
const connectionString = {
  development: process.env.DB_CONNECTION || 'mongodb://localhost/gdd_development' ,
  test: process.env.DB_CONNECTION_TEST,
  staging: process.env.DB_CONNECTION,
  production: process.env.DB_CONNECTION,
}

mongoose.connect(connectionString[process.env.NODE_ENV || 'development'], {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true
})
  .then(() => "Database Connected")
  .catch(err => {
    console.error(err.message);
    process.exit;
  })
