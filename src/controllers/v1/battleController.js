const BattleHistory = require('../../models/battleHistory.js');;
const authenticate = require('../../middlewares/authenticate.js');
const faker = require('faker');

const resources = [
  {
    method: "POST",
    middlewares: [authenticate],
    params: {
      'Authorization': {
        type: 'string',
        required: true,
        in: 'header',
        description: 'Example: Bearer eyJfaWQiOiI1...'
      },
      mode: {
        type: 'string',
        enum: ['Singleplayer', 'Multiplayer'],
        in: 'body',
        required: true
      },
      result: {
        type: 'string',
        enum: [
          'Player Win',
          'Opponent Win',
          'Draw'
        ],
        in: 'body',
        description: "Test",
        required: true
      }
    },
    path: "/",
    handler: async function(req, res, next) {
      try {
        let instance = await BattleHistory.create({ player: req.user, ...req.body });

        req.body = [
          true,
          instance.entity,
          200
        ]
      }

      catch(err) {
        req.body = [false, err.msg, 422]
      }

      next()
    }
  },
  {
    method: "GET",
    middlewares: [authenticate],
    params: {
      'Authorization': {
        type: 'string',
        required: true,
        in: 'header',
        description: 'Example: Bearer eyJfaWQiOiI1...'
      },
    },
    path: "/",
    handler: async function(req, res, next) {
      try {
        req.body = [true, await BattleHistory.getResult(req.user), 200]
      }

      catch(err) {
        req.body = [false, err.message, 422];
      }

      next()
    }
  }
]

module.exports = {
  namespace: "/battle",
  resources
}
