// Dependencies
const User = require('../../models/user.js');
const faker = require('faker');

// Middlewares
const authenticate = require('../../middlewares/authenticate.js');

var resources = [
  {
    method: "POST",
    path: "/register",
    params: {
      email: {
        type: 'email',
        required: true,
        example: faker.internet.email(),
        in: 'body'
      },
      username: {
        type: 'string',
        required: true,
        example: faker.internet.userName(),
        in: 'body'
      },
      password: {
        type: 'string',
        required: true,
        example: faker.internet.password(),
        in: 'body'
      },
    },
    handler: async function(req, res, next) {
      try {
        req.body = [true, await User.register(req.body), 201]
      }

      catch(err) {
        req.body = [false, err.message, 422];
      }

      next()
    }
  },
  {
    method: "POST",
    path: "/login",
    params: {
      email: {
        type: 'email',
        required: true,
        example: faker.internet.email(),
        in: 'body'
      },
      password: {
        type: 'string',
        required: true,
        example: faker.internet.password(),
        in: 'body'
      },
    },
    handler: async function(req, res, next) {
      try { req.body = [true, await User.authenticate(req.body), 200] }
      catch(err) { req.body = [false, err.message, 401] }
      next()
    }
  },
  {
    method: "GET",
    path: "/me",
    middlewares: [authenticate],
    params: {
      'Authorization': {
        type: 'string',
        required: true,
        in: 'header',
        description: 'Example: Bearer eyJfaWQiOiI1...'
      }
    },
    handler: async function(req, res, next) {
      try { req.body = [true, req.user.entity, 200] }
      catch(err) { req.body = [false, err.message, 404] }
      next()
    }
  }
]

module.exports = {
  namespace: "/auth",
  resources
}
