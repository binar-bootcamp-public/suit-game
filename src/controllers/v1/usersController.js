const User = require('../../models/user.js');
const authenticate = require('../../middlewares/authenticate.js');

var resources = [
  {
    method: "GET",
    middlewares: [authenticate],
    params: {
      'Authorization': {
        type: 'string',
        required: true,
        in: 'header',
        description: 'Example: Bearer eyJfaWQiOiI1...'
      }
    },
    path: "/",
    handler: async function(req, res, next) {
      try { req.body = [true, req.user.profile, 200] }
      catch(err) { req.body = [false, err.message, 422] }
      next()
    }
  },

  {
    method: "PUT",
    path: "/",
    middlewares: [authenticate],
    params: {
      'Authorization': {
        type: 'string',
        required: true,
        in: 'header',
        description: 'Example: Bearer eyJfaWQiOiI1...'
      },

      username: {
        type: 'string',
        optional: true,
        in: 'formData'
      },

      email: {
        type: 'string',
        optional: true,
        in: 'formData'
      },

      photo: {
        type: 'file',
        optional: true,
        in: 'formData'
      }
    },
    handler: async function(req, res, next) {
      try { 
        if (Object.keys(req.files).length !== 0) req.body.photo = req.files.photo[0];
        await req.user.edit(req.body);

        req.body = [true, req.user.profile, 200] 
      }
      catch(err) { req.body = [false, err.message, 422] }
      next()
    }
  },
]

module.exports = {
  namespace: "/users",
  resources
}
