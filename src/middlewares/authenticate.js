const jwt = require('jsonwebtoken');
const User = require('../models/user.js');

module.exports = async (req, res, next) => {
  try {
    const token = req.headers.authorization.split("Bearer ")[1];
    req.headers.authorization = jwt.verify(token, process.env.SECRET_KEY);
    req.user = await User.findById(req.headers.authorization._id); 
    next()
  }

  catch(err) {
    if (err.name === 'TokenExpiredError')
      return res.status(403).json({
        status: false,
        errors: 'Token is expired'
      })

    res.status(401).json({
      status: false,
      errors: "Invalid Token"
    })
  }
}
