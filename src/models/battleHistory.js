const mongoose = require('mongoose');
const { Schema } = mongoose; 

const schema = new Schema(
  {
    player: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    result: {
      type: 'string',
      enum: [
        'Player Win',
        'Opponent Win',
        'Draw'
      ],
      required: true
    },
    mode: {
      type: 'string',
      enum: [
        'Multiplayer',
        'Singleplayer'
      ],
      required: true
    }
  },

  {
    versionKey: false,
    timestamps: true,
    collection: 'battle_histories'
  }
)

const BattleHistory = mongoose.model('BattleHistory', schema);

BattleHistory.getResult = async function(user) {
  try {
    let instances = await this
      .find({ player: user._id })
      .populate('player');
    return Promise.resolve(
      instances.map(
        instance => instance.format()
      )
    );
  }

  catch(err) {
    return Promise.reject(err)
  }
}

BattleHistory.prototype.format = function() {
  return {
    _id: this._id,
    result: this.result,
    mode: this.mode,
    message: this.setMessage(),
    createdAt: this.createdAt,
    updatedAt: this.updatedAt
  }
}

BattleHistory.prototype.setMessage = function() {
  switch(this.result) {
    case 'Player Win':
      msg = `${this.player.username} Menang`;
      break;

    case 'Opponent Win':
      msg = `${this.player.username} Kalah`;
      break;

    case 'Draw':
      msg = `${this.player.username} Seri`
      break;

    default:
      throw new Error("Not a valid result typep")
  }

  return this.message = msg;
}

Object.defineProperty(BattleHistory.prototype, 'entity', {
  get() {
    return {
      _id: this._id,
      result: this.result,
      mode: this.mode,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }
  }
})


module.exports = BattleHistory;
