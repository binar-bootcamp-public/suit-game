const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const imagekit = require('../module/imagekit.js');
const { Schema } = mongoose; 

const userSchema = new Schema({

  username: {
    type: 'string',
    required: true,
    unique: true,
    lowercase: true,
    minlength: 6,
    validate: {
      validator(v) {
        return /^[a-zA-Z0-9]*$/.test(v) 
      },
      message: props => `${props.value} should only contain alphanumeric characters`
    }
  },

  email: {
    type: 'string',
    required: true,
    unique: true,
    lowercase: true
  },

  encryptedPassword: {
    type: 'string',
    required: true
  },

  photo: {
    type: 'string',
    optional: true
  }
})

function generateToken(payload) {
  return jwt.sign(payload, process.env.SECRET_KEY);
}

const User = mongoose.model('User', userSchema)

async function upload(photo) {
  let image = await imagekit.upload({
    file: photo.buffer.toString('base64'),
    fileName: `IMG-${Date.now()}`
  })

  return Promise.resolve(image.url);
}

// Static Method
User._encrypt = function(password) {
  return bcrypt.hashSync(password, 10);
}

User.register = async function({ username, email, password }) {
  let instance = await this.create({
    username,
    email,
    encryptedPassword: await this._encrypt(password)
  })

  return Promise.resolve(
    instance.entity
  )
}

User.authenticate = async function({ email, username, password }) {
  let query = {}
  query[email ? "email" : "username"] = email || username;

  let instance = await this.findOne(query)
  if (!instance) throw new Error("Email doesn't exist!")
  if (instance.isPasswordCorrect(password))
    return Promise.resolve(instance.identity)
  
  return Promise.reject(new Error("Wrong password!"))
}
// End of Static Method

// Instance Method
User.prototype.generateToken = function() {
  return jwt.sign(this.entity, process.env.SECRET_KEY, { expiresIn: '2h' });
}

User.prototype.isPasswordCorrect = function(password) {
  return bcrypt.compareSync(password, this.encryptedPassword)
}

Object.defineProperties(User.prototype, {
  
  profile: {
    get() {
      return {
        ...this.entity,
        photo: this.photo
      }
    }
  },

  entity: {
    get() {
      const { _id, username, email } = this;

      return {
        _id,
        username,
        email,
      }
    }
  },

  identity: {
    get() {
      return {
        ...this.entity,
        token: this.generateToken()
      }
    }
  }

})

User.prototype.setPhoto = async function(photo) {
  try {
    this.photo = await upload(photo); 
    return promise.resolve(await this.save())
  } catch(err) {
    return Promise.reject(err);
  }
}

User.prototype.edit = async function(data) {
  delete data.encryptedPassword;
  delete data._id;

  for (let i in data) {
    if (!data[i] || Object.keys(data[i]).length == 0)
      return Promise.reject(new Error(`${i} cannot be blank once you've sent it!`))
  }

  try {
    if (data.photo) 
      data.photo = await upload(data.photo);

    Object.assign(this, data);
    return Promise.resolve(await this.save())
  }

  catch(err) {
    return Promise.reject(err);
  }
}
// End of Shape the Object

module.exports = User;
